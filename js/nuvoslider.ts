class NuvoSlider {
    private _slide:any;
    private _slider:any;
    private _img:any;
    private _sliderHeight:number;
    private _arrows:any;
    private _starter:any;
    private _slideNumber:number = 0;
    private _touchClick:number = 0;
    private _time:number = 5000;

    constructor(time:number) {
        this.time = time;
    }

    public initialize = () => {
        this.slide = document.querySelectorAll('.slide');
        this.slide[0].className = "slide first-active";
        this.img = document.querySelectorAll('.slide img');
        this.img[0].addEventListener('load', this.resize); /* force first image resize on load. other images are resized with setNextSlide or on window's resize.*/
        this.makeImagesInvisible(); /*in case user add images with different heights.*/
        this.arrows = document.querySelectorAll('.slider-control');
        this.slider = document.querySelector('.nuvoslider');
        this.resize();
        this.enableTouch();
        this.start();
        window.addEventListener("resize", this.resize, false);
        document.querySelector(".slider-previous").addEventListener("click", this.sliderBackward, false);
        document.querySelector(".slider-next").addEventListener("click", this.forward, false);
    };

    public enableTouch = () => {
        /*FOR TOUCH SCREEN DEVICES*/
        (function (d) {
            var ce = function (e, n) {
                    var a = document.createEvent("CustomEvent");
                    a.initCustomEvent(n, true, true, e.target);
                    e.target.dispatchEvent(a);
                    a = null;
                    return false
                },
                nm = true, sp = {x: 0, y: 0}, ep = {x: 0, y: 0},
                touch = {
                    touchstart: function (e) {
                        sp = {x: e.touches[0].pageX, y: e.touches[0].pageY}
                    },
                    touchmove: function (e) {
                        nm = false;
                        ep = {x: e.touches[0].pageX, y: e.touches[0].pageY}
                    },
                    touchend: function (e) {
                        if (nm) {
                            ce(e, 'fc')
                        } else {
                            var x = ep.x - sp.x, xr = Math.abs(x), y = ep.y - sp.y, yr = Math.abs(y);
                            if (Math.max(xr, yr) > 20) {
                                ce(e, (xr > yr ? (x < 0 ? 'swl' : 'swr') : (y < 0 ? 'swu' : 'swd')))
                            }
                        }
                        ;
                        nm = true
                    },
                    touchcancel: function (e) {
                        nm = false
                    }
                };
            for (var a in touch) {
                d.addEventListener(a, touch[a], false);
            }
        })(document);
        var h = function (e) {
            console.log(e.type, e)
        };
        document.body.addEventListener('fc', this.toggleArrows, false); // 0-50ms = fast-clicks vs 500ms for normal clicks
        document.body.addEventListener('swl', this.forward, false);
        document.body.addEventListener('swr', this.sliderBackward, false);
    };

    public isHover(e) {
        return (e.parentElement.querySelector(':hover') === e);
    };

    public toggleArrows = () => {
        if (this.isHover(document.querySelector(".slider-previous")) || this.isHover(document.querySelector(".slider-next"))) {
            this.arrows[0].style.opacity = "1";
            this.arrows[1].style.opacity = "1";
        } else {
            if (this.touchClick == 2) {
                this.touchClick = 0
            }
            if (this.touchClick % 2 == 0) {
                this.arrows[0].style.opacity = "1";
                this.arrows[1].style.opacity = "1";
            } else {
                this.arrows[0].style.opacity = "0";
                this.arrows[1].style.opacity = "0";
            }
            this.touchClick++;
        }
    };

    public makeImagesInvisible = () => { /*makes images (but first) invisible in case subsequent images have different heights*/
        for (var i = 1; i < this.img.length; i++) {
            this.img[i].style.opacity = "0";
        }
    };

    public start = () => {
        this.starter = setInterval(this.sliderForward, this.time);
    };

    public restart = () => {
        clearInterval(this.starter);
        this.start();
    };

    public forward = () => {
        this.restart();
        this.sliderForward();
    };

    public resize = () => { //resize slider height to equal the height of the image
        this.sliderHeight = this.img[this.slideNumber].clientHeight;
        this.slider.style.height = this.sliderHeight + "px";
    };

    public setNextSlide = () => {
        this.img[this.slideNumber].removeAttribute("style", "opacity: 0;");
        this.slide[this.slideNumber].className = "slide active";
        this.resize();
    };

    public sliderForward = () => {
        this.slide[this.slideNumber].className = "slide";
        if (this.slideNumber == this.slide.length - 1) {
            this.slideNumber = 0;
        } else {
            this.slideNumber++;
        }
        this.setNextSlide();
    };

    public sliderBackward = () => {
        this.restart();
        this.slide[this.slideNumber].className = "slide";
        this.slideNumber--;
        if (this.slideNumber == -1) {
            this.slideNumber = this.slide.length - 1;
        }
        this.setNextSlide();
    };

    get slide():any {
        return this._slide;
    }

    set slide(slide:any) {
        this._slide = slide;
    }

    get slider():any {
        return this._slider;
    }

    set slider(slider:any) {
        this._slider = slider;
    }

    get img():any {
        return this._img;
    }

    set img(img:any) {
        this._img = img;
    }

    get sliderHeight():number {
        return this._sliderHeight;
    }

    set sliderHeight(sliderHeight:number) {
        this._sliderHeight = sliderHeight;
    }

    get arrows():any {
        return this._arrows;
    }

    set arrows(arrows:any) {
        this._arrows = arrows;
    }

    get starter():any {
        return this._starter;
    }

    set starter(starter:any) {
        this._starter = starter;
    }

    get slideNumber():number {
        return this._slideNumber;
    }

    set slideNumber(slideNumber:number) {
        this._slideNumber = slideNumber;
    }

    get touchClick():number {
        return this._touchClick;
    }

    set touchClick(touchClick:number) {
        this._touchClick = touchClick;
    }

    get time():number {
        return this._time;
    }

    set time(time:number) {
        if (time > 0) {
            this._time = time;
        } else {
            console.log("error, time entered cannot be negative");
            this._time = 5000;
        }
    }

}