var NuvoSlider = (function () {
    function NuvoSlider(time) {
        var _this = this;
        this._slideNumber = 0;
        this._touchClick = 0;
        this._time = 5000;
        this.initialize = function () {
            _this.slide = document.querySelectorAll('.slide');
            _this.slide[0].className = "slide first-active";
            _this.img = document.querySelectorAll('.slide img');
            _this.img[0].addEventListener('load', _this.resize); /* force first image resize on load. other images are resized with setNextSlide or on window's resize.*/
            _this.makeImagesInvisible(); /*in case user add images with different heights.*/
            _this.arrows = document.querySelectorAll('.slider-control');
            _this.slider = document.querySelector('.nuvoslider');
            _this.resize();
            _this.enableTouch();
            _this.start();
            window.addEventListener("resize", _this.resize, false);
            document.querySelector(".slider-previous").addEventListener("click", _this.sliderBackward, false);
            document.querySelector(".slider-next").addEventListener("click", _this.forward, false);
        };
        this.enableTouch = function () {
            /*FOR TOUCH SCREEN DEVICES*/
            (function (d) {
                var ce = function (e, n) {
                    var a = document.createEvent("CustomEvent");
                    a.initCustomEvent(n, true, true, e.target);
                    e.target.dispatchEvent(a);
                    a = null;
                    return false;
                }, nm = true, sp = { x: 0, y: 0 }, ep = { x: 0, y: 0 }, touch = {
                    touchstart: function (e) {
                        sp = { x: e.touches[0].pageX, y: e.touches[0].pageY };
                    },
                    touchmove: function (e) {
                        nm = false;
                        ep = { x: e.touches[0].pageX, y: e.touches[0].pageY };
                    },
                    touchend: function (e) {
                        if (nm) {
                            ce(e, 'fc');
                        }
                        else {
                            var x = ep.x - sp.x, xr = Math.abs(x), y = ep.y - sp.y, yr = Math.abs(y);
                            if (Math.max(xr, yr) > 20) {
                                ce(e, (xr > yr ? (x < 0 ? 'swl' : 'swr') : (y < 0 ? 'swu' : 'swd')));
                            }
                        }
                        ;
                        nm = true;
                    },
                    touchcancel: function (e) {
                        nm = false;
                    }
                };
                for (var a in touch) {
                    d.addEventListener(a, touch[a], false);
                }
            })(document);
            var h = function (e) {
                console.log(e.type, e);
            };
            document.body.addEventListener('fc', _this.toggleArrows, false); // 0-50ms = fast-clicks vs 500ms for normal clicks
            document.body.addEventListener('swl', _this.forward, false);
            document.body.addEventListener('swr', _this.sliderBackward, false);
        };
        this.toggleArrows = function () {
            if (_this.isHover(document.querySelector(".slider-previous")) || _this.isHover(document.querySelector(".slider-next"))) {
                _this.arrows[0].style.opacity = "1";
                _this.arrows[1].style.opacity = "1";
            }
            else {
                if (_this.touchClick == 2) {
                    _this.touchClick = 0;
                }
                if (_this.touchClick % 2 == 0) {
                    _this.arrows[0].style.opacity = "1";
                    _this.arrows[1].style.opacity = "1";
                }
                else {
                    _this.arrows[0].style.opacity = "0";
                    _this.arrows[1].style.opacity = "0";
                }
                _this.touchClick++;
            }
        };
        this.makeImagesInvisible = function () {
            for (var i = 1; i < _this.img.length; i++) {
                _this.img[i].style.opacity = "0";
            }
        };
        this.start = function () {
            _this.starter = setInterval(_this.sliderForward, _this.time);
        };
        this.restart = function () {
            clearInterval(_this.starter);
            _this.start();
        };
        this.forward = function () {
            _this.restart();
            _this.sliderForward();
        };
        this.resize = function () {
            _this.sliderHeight = _this.img[_this.slideNumber].clientHeight;
            _this.slider.style.height = _this.sliderHeight + "px";
        };
        this.setNextSlide = function () {
            _this.img[_this.slideNumber].removeAttribute("style", "opacity: 0;");
            _this.slide[_this.slideNumber].className = "slide active";
            _this.resize();
        };
        this.sliderForward = function () {
            _this.slide[_this.slideNumber].className = "slide";
            if (_this.slideNumber == _this.slide.length - 1) {
                _this.slideNumber = 0;
            }
            else {
                _this.slideNumber++;
            }
            _this.setNextSlide();
        };
        this.sliderBackward = function () {
            _this.restart();
            _this.slide[_this.slideNumber].className = "slide";
            _this.slideNumber--;
            if (_this.slideNumber == -1) {
                _this.slideNumber = _this.slide.length - 1;
            }
            _this.setNextSlide();
        };
        this.time = time;
    }
    NuvoSlider.prototype.isHover = function (e) {
        return (e.parentElement.querySelector(':hover') === e);
    };
    ;
    Object.defineProperty(NuvoSlider.prototype, "slide", {
        get: function () {
            return this._slide;
        },
        set: function (slide) {
            this._slide = slide;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NuvoSlider.prototype, "slider", {
        get: function () {
            return this._slider;
        },
        set: function (slider) {
            this._slider = slider;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NuvoSlider.prototype, "img", {
        get: function () {
            return this._img;
        },
        set: function (img) {
            this._img = img;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NuvoSlider.prototype, "sliderHeight", {
        get: function () {
            return this._sliderHeight;
        },
        set: function (sliderHeight) {
            this._sliderHeight = sliderHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NuvoSlider.prototype, "arrows", {
        get: function () {
            return this._arrows;
        },
        set: function (arrows) {
            this._arrows = arrows;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NuvoSlider.prototype, "starter", {
        get: function () {
            return this._starter;
        },
        set: function (starter) {
            this._starter = starter;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NuvoSlider.prototype, "slideNumber", {
        get: function () {
            return this._slideNumber;
        },
        set: function (slideNumber) {
            this._slideNumber = slideNumber;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NuvoSlider.prototype, "touchClick", {
        get: function () {
            return this._touchClick;
        },
        set: function (touchClick) {
            this._touchClick = touchClick;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NuvoSlider.prototype, "time", {
        get: function () {
            return this._time;
        },
        set: function (time) {
            if (time > 0) {
                this._time = time;
            }
            else {
                console.log("error, time entered cannot be negative");
                this._time = 5000;
            }
        },
        enumerable: true,
        configurable: true
    });
    return NuvoSlider;
}());
//# sourceMappingURL=nuvoslider.js.map